import os
import random
import base64
import json
import requests
import urllib.parse as urlParser


"""
Skript zum Einloggen in einen pr0gramm Account. Benötigt eine Datei 'credentials.json' mit Nutzernamen 
und Passwort ({"name": "[Nutzername]", "password": "[Passwort]"}). Speichert Cookies und Nonce anschließend
in einer Datei 'login_data.json'.  
"""


def main():
    with open("credentials.json", "r") as credentials_json:
        credentials = json.load(credentials_json)
    bust = random.random()
    login_request = requests.get(f"https://pr0gramm.com/api/user/captcha?bust={bust}")
    token = login_request.json().get("token")
    captcha_data = login_request.json().get("captcha")[22:]
    with open("captcha.png", "wb") as captcha_writer:
        captcha_writer.write(base64.decodebytes(captcha_data.encode()))
    captcha_plain = input("Successfully created 'captcha.png' in the current directory. Enter captcha content: ")
    os.remove("captcha.png")
    form_data = credentials | {"token": token, "captcha": captcha_plain}
    response = requests.post("https://pr0gramm.com/api/user/login", data=form_data)
    if not response.json().get("success"):
        print("Login failed. Check 'credentials.json' and make sure you enter the correct captcha.")
    else:
        print("Login successful. Saving cookies to 'login_data.json' in the current directory.")
        me_cookie = response.cookies.get("me")
        pp_cookie = response.cookies.get("pp")
        nonce = json.loads(urlParser.unquote(me_cookie)).get("id")[:16]
        login_data = {
            "me": me_cookie,
            "pp": pp_cookie,
            "nonce": nonce
        }
        with open("login_data.json", "w") as output_file:
            json.dump(login_data, output_file)


if __name__ == "__main__":
    main()
