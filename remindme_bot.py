import pr0pi as ppi
import reminder as rmn
import sys
import logging
import signal
import atexit
import time
import csv


"""
Erinnerungsbot für pr0gramm
"""


def main():

    logging.basicConfig(filename="bot.log",
                        format="%(asctime)s - %(message)s",
                        datefmt="%d-%b-%y %H:%M:%S",
                        level=logging.INFO)

    # holt wenn möglich alle offenen Reminder aus einer entsprechenden .csv Datei
    reminders = list()
    try:
        with open("pending_reminders.csv") as csvfile:
            csvreader = csv.reader(csvfile)
            for row in csvreader:
                row[:2] = list(map(int, row[:2]))
                row[3:] = list(map(int, row[3:]))
                row = tuple(row)
                reminders.append(row)
        logging.info("Successfully imported 'pending_reminders.csv'")
    except FileNotFoundError:
        logging.info("'pending_reminders.csv' does not exist (yet). Starting with a fresh list")

    # da signal handler keine eigenen Argumente entgegen nehmen, werden SIGINT/SIGTERM mithilfe des handlers
    # in sys.exit() "umgewandelt", was wiederum von atexit abgefangen wird um die Reminder zu speichern
    signal.signal(signal.SIGINT, handler)
    signal.signal(signal.SIGTERM, handler)
    atexit.register(shutdown, reminders)

    while True:

        # holt neue Markierungen und fügt ggf. Reminder zur Liste hinzu
        messages = None
        while messages is None:
            messages = ppi.get_messages()
            time.sleep(5)

        for message in messages:
            if message.get("read"):
                break
            remind_time = rmn.get_remind_time(message)
            if remind_time is not None:
                reminder = rmn.create_reminder(message, remind_time)
                reminders.append(reminder)
                reminders.sort(key=lambda l: l[0])
                logging.info(f"[CREATED] reminder for user '{reminder[2]}', to comment pr0gramm.com/new/{reminder[3]}:comment{reminder[4]}")
            else:
                rmn.send_instructions(message)
                user = message.get("name")
                logging.info(f"[SENT] instructions for user '{user}'")

        # schickt fällige Erinnerungen und löscht diese aus der Liste
        current_time = int(time.time())
        ready_to_send = [reminder for reminder in reminders if current_time > reminder[0]]
        for reminder in ready_to_send:
            rmn.send_reminder(*reminder)
            reminders.pop(0)
            logging.info(f"[SENT] reminder for user '{reminder[2]}', to comment pr0gramm.com/new/{reminder[3]}:comment{reminder[4]}")

        time.sleep(30)


# wird bei SIGINT/SIGTERM aufgerufen
def handler(signum, frame):
    sys.exit()


# speichert alle offenen Reminder in einer .csv Datei
def shutdown(reminder_list):
    logging.info("Saving to 'pending_reminders.csv'")
    with open("pending_reminders.csv", "w") as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerows(reminder_list)
    logging.info("Shutting down")


if __name__ == "__main__":
    main()
