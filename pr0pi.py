import requests
import json


def get_login_data():
    """
    Holt Cookies und Nonce aus einer JSON-Datei
    :return: Cookies und Nonce in Form eines Dictionarys
    """
    with open("login_data.json", "r") as ld:
        login_data = json.load(ld)
    return login_data


def get_messages():
    """
    Holt die Nachrichten des Botaccounts
    :return: alle Nachrichten (Markierungen) in Form eines JSON-Objekts
    """
    my_cookies = get_login_data()
    my_cookies.pop("nonce")
    try:
        r = requests.get("https://pr0gramm.com/api/inbox/comments", cookies=my_cookies)
        return r.json().get("messages")
    except:  # todo besseres exception handling
        return None


def send_message(content, recipient):
    """
    Schickt eine PN an einen Nutzer
    :param content: Nachricht in Form eines Strings
    :param recipient: Nutzername in Form eines Strings
    :return: Antwort der POST-Request in Form eines JSON-Objekts
    """
    my_cookies = get_login_data()
    nonce = my_cookies.pop("nonce")
    form_data = {
        "comment": content,
        "recipientName": recipient,
        "_nonce": nonce
    }
    try:
        r = requests.post("https://pr0gramm.com/api/inbox/post", data=form_data, cookies=my_cookies)
        return r.json()
    except:  # todo besseres exception handling
        return None
