from datetime import datetime
import re
import pr0pi as ppi


def remind_time_from_period(initial, period):
    """
    Berechnet den Zeitpunkt einer Erinnerung aus einem Startwert und einer gegebenen Zeitspanne
    :param initial: Zeitpunkt der Markierung in Form eines UNIX-Timestamps
    :param period: Zeitspanne, nach der erinnert werden soll ([0-99][hdw])
    :return: Zeitpunkt der Erinnerung in Form eines UNIX-Timestamps
    """
    amount = int(period[:-1])
    unit = period[-1]
    units = {
        "h": 3600,
        "d": 86400,
        "w": 604800
    }
    return initial + amount * units.get(unit)


def remind_time_from_datetime(dt_lst):
    """
    Berechnet den Zeitpunkt einer Erinnerung aus einer Datums- und optionalen Uhrzeitangabe
    :param dt_lst: Liste mit Datums- und optionaler Uhrzeitangabe ([yyyy-mm-dd][hh:mm])
    :return: Zeitpunkt der Erinnerung in Form eines UNIX-Timestamps
    """
    dt_str = " ".join(dt_lst)
    try:
        dt_o = datetime.fromisoformat(dt_str)
        ts = int(dt_o.timestamp())
    except ValueError:
        if dt_str[:4] != "0000":
            return None
        else:
            return 0
    except OverflowError:
        return 0
    return ts


def convert_de_to_iso(dt_lst):
    """
    Konvertiert das deutsche Datumsformat zum ISO 8601 Format
    :param dt_lst: Liste mit Datums- und optionaler Uhrzeitangabe im deutschen Format ([dd.mm.yyyy][hh:mm])
    :return: Liste mit Datums- und optionaler Uhrzeitangabe im ISO 8601 Format ([yyyy-mm-dd][hh:mm])
    """
    values = [x for x in reversed(dt_lst[0].split("."))]
    dt_lst[0] = "-".join(values)
    return dt_lst


def calc_elapsed_time(start, end):
    """
    Berechnet die verstrichene Zeit seit einer Markierung
    :param start: Zeitpunkt der Markierung in Form eines UNIX-Timestamps
    :param end: Zeitpunkt der Erinnerung in Form eines UNIX-Timestamps
    :return: Tupel mit der Anzahl verstrichener Jahre, Wochen, Tage, Stunden
    """
    elapsed = end - start
    y = elapsed // 31536000
    w = elapsed % 31536000 // 604800
    d = elapsed % 604800 // 86400
    h = elapsed % 86400 // 3600
    m = elapsed % 3600 // 60
    return y, w, d, h, m


def get_remind_time(comment):
    """
    Durchsucht einen Kommentar nach einer gültigen Zeitangabe
    :param comment: unveränderter Kommentar in Form des JSON-Objekts
    :return: Zeitpunkt der Erinnerung in Form eines UNIX-Timestamps
    """
    content = comment.get("message").lower()
    p = re.search("@vergissmichnicht\s+\d{1,2}[hdw]", content)
    dt = re.search("@vergissmichnicht\s+\d{4}-\d{2}-\d{2}(\s+\d{2}:\d{2})?", content)
    dtg = re.search("@vergissmichnicht\s+\d{2}.\d{2}.\d{4}(\s+\d{2}:\d{2})?", content)
    if p is not None:
        p = p.group().split()[1]
        timestamp = comment.get("created")
        return remind_time_from_period(timestamp, p)
    elif dt is not None:
        dt = dt.group().split()[1:]
        return remind_time_from_datetime(dt)
    elif dtg is not None:
        dtg = dtg.group().split()[1:]
        dt = convert_de_to_iso(dtg)
        return remind_time_from_datetime(dt)
    else:
        return None


def create_reminder(comment, remind_time):
    """
    Erstellt einen Reminder in Form eines Tupels mit allen benötigten Werten
    :param comment: unveränderter Kommentar in Form des JSON-Objekts
    :param remind_time: Zeitpunkt der Erinnerung in Form eines UNIX-Timestamps
    :return: Tupel mit Erinnerungszeitpunkt, Markierungszeitpunkt, Nutzer, Item ID und Kommentar ID
    """
    timestamp = comment.get("created")
    recipient = comment.get("name")
    iid = comment.get("itemId")
    cid = comment.get("id")
    return remind_time, timestamp, recipient, iid, cid


def send_reminder(remind_time, timestamp, recipient, item_id, comment_id):
    """
    Erstellt eine Erinnerungsnachricht und schickt diese an den entsprechenden Nutzer
    :param remind_time: Zeitpunkt der Erinnerung in Form eines UNIX-Timestamps
    :param timestamp: Zeitpunkt der Markierung in Form eines UNIX-Timestamps
    :param recipient: Nutzername in Form eines Strings
    :param item_id: Item ID des Uploads, unter dem die Markierung stattfand
    :param comment_id: Kommentar ID der Markierung
    :return: Antwort der POST-Request in Form eines JSON-Objekts
    """
    if remind_time > timestamp:
        elapsed = calc_elapsed_time(timestamp, remind_time)
        if elapsed != (0, 0, 0, 0, 0):
            units = ["Jahr(en)", "Woche(n)", "Tag(en)", "Stunde(n)", "Minute(n)"]
            gtzero = [f"{v} {units[i]}" for i, v in enumerate(elapsed) if v != 0]
            period = f",\nnach {', '.join(gtzero)}"
        else:
            period = str()
    else:
        return send_rage(recipient)
    ts_plain = datetime.fromtimestamp(timestamp)
    msg = f"Du hast mich am {ts_plain} im verlinkten Kommentar markiert, um dich jetzt{period} zu erinnern:\n" \
          f"https://pr0gramm.com/new/{item_id}:comment{comment_id} "
    response = None
    while response is None:
        response = ppi.send_message(msg, recipient)
    return response


def send_instructions(comment):
    """
    Schickt nach fehlerhafter Markierung eine Benachrichtigung an den entsprechenden Nutzer
    :param recipient: Nutzername in Form eines Strings
    :return: Antwort der POST-Request in Form eines JSON-Objekts
    """
    recipient = comment.get("name")
    item_id = comment.get("itemId")
    comment_id = comment.get("id")

    msg = f"Du scheinst mich mit Kommentar pr0gramm.com/new/{item_id}:comment{comment_id} markiert zu haben, ohne einen gültigen Erinnerungszeitpunkt angegeben zu haben. " \
          "Zulässige Formate sind:\n" \
          "[1-99]h/d/w, " \
          "yyyy-mm-dd bzw. yyyy-mm-dd hh:mm oder " \
          "dd.mm.yyyy bzw. dd.mm.yyyy hh:mm\n" \
          "\nBeispielmarkierungen:\n" \
          "'@VergissMichNicht 1w'\n" \
          "'@VergissMichNicht 2007-01-23'\n" \
          "'@VergissMichNicht 2007-01-23 21:39'\n" \
          "'@VergissMichNicht 23.01.2007'\n" \
          "'@VergissMichNicht 23.01.2007 21:39'"
    response = None
    while response is None:
        response = ppi.send_message(msg, recipient)
    return response


def send_rage(recipient):
    msg = bytes.fromhex("4d6163682064696368207261757320617573206d65696e65204c656974756e672c206475204269726e6521")\
        .decode("utf-8")
    response = None
    while response is None:
        response = ppi.send_message(msg, recipient)
    return response
